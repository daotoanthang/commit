import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent {
  @Input() item = "";
  @Output() newItemEvent = new EventEmitter<string>();
}
