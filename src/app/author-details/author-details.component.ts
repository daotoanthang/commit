import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Author } from '../authors';

@Component({
  selector: 'app-author-details',
  templateUrl: './author-details.component.html',
  styleUrls: ['./author-details.component.scss'],
})
export class AuthorDetailsComponent {
  @Input() author: Author;
  @Output() deleteAuthor = new EventEmitter<Author>();
  handleDelete() {
    this.deleteAuthor.emit(this.author);
  }
}
