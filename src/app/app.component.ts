import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Cloudinary, CloudinaryVideo } from '@cloudinary/url-gen';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  vid!: CloudinaryVideo;
  title = 'myApp';
  ngOnInit(): void {
    
    const cld = new Cloudinary({
      cloud: {
        cloudName: 'demo',
      },
    });
  }
  public loginName = "adm323232in";
  public currentItem = "table";
  /////
  player: any;
  public id: string = 'qDuKsiwS5xw';
  savePlayer(player: any) {
    this.player = player;
    console.log(player);
  }

  playVideo() {
    this.player.playVideo();
  }
  pauseVideo() {
    this.player.pauseVideo();
  }

  @ViewChild('myVideo') myVideo: ElementRef;

  isPlaying: boolean = false;

  play() {
    this.myVideo.nativeElement.play();
    this.isPlaying = true;
  }
  pause() {
    this.myVideo.nativeElement.pause();
    // this.myVideo.nativeElement.currentTime = 0;
    this.isPlaying = false;
  }
  
  
}
